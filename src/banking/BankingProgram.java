package banking;

import java.util.Objects;
import java.util.Scanner;

public class BankingProgram {

    public static void main(String[] args) {

        CreditCardAccount creditCard = new CreditCardAccount();
        creditCard.setNumber(9999);
        creditCard.setPin(1213);

        BankingProcess process = new BankingProcess(creditCard);
        process.start();

        /*if (Objects.equals(creditCard.getNumber(), Database.INSTANCE.getInstance().getNumber())) {
            creditCard = Database.INSTANCE.getInstance();
        }*/
    }
}
