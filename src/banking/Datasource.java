package banking;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Datasource {
    public static CreditCardAccount creditCard() {
        CreditCardAccount creditCardAccount = new CreditCardAccount("Reksmey", 9999, 1000.000);
        creditCardAccount.setOverdraft(true);
        creditCardAccount.setPin(1213);
        creditCardAccount.setIssuedAt(LocalDate.now());
        creditCardAccount.setThruAt(LocalDate.now().plus(2, ChronoUnit.DAYS));
        return creditCardAccount;
    }

}
