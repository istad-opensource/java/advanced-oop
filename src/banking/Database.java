package banking;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public enum Database {
    INSTANCE;
    private CreditCardAccount creditCard;
    public CreditCardAccount getInstance() {
        if (creditCard == null) {

            CreditCardAccount creditCardAccount = new CreditCardAccount("Reksmey", 9999, 1000.000);
            creditCardAccount.setOverdraft(true);
            creditCardAccount.setPin(1213);
            creditCardAccount.setIssuedAt(LocalDate.now());
            creditCardAccount.setThruAt(LocalDate.now().plus(2, ChronoUnit.DAYS));

            creditCard = creditCardAccount;
        }
        return creditCard;
    }
}
