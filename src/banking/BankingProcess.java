package banking;

import java.io.IOException;
import java.util.Scanner;

public class BankingProcess {

    private int option;
    private final boolean isProcessing;
    private final Scanner scanner;
    private final CreditCardAccount creditCard;

    public BankingProcess(CreditCardAccount creditCard) {
        this.scanner = new Scanner(System.in);
        this.creditCard = creditCard;
        this.isProcessing = true;
    }

    public void getOption() {
        System.out.println("Welcome to Bank Program");
        System.out.println("-----------------------");
        System.out.println("1. Show Balance");
        System.out.println("2. Deposit");
        System.out.println("3. Withdrawal");
        System.out.println("4. Exit");
        System.out.println("-----------------------");
        System.out.print("* Choose option -> ");
        option = scanner.nextInt();
    }

    public void doProcess() {
        switch (option) {
            case 1: {
                creditCard.showBalance();
                break;
            }
            case 2: {
                System.out.print("Enter deposit amount -> ");
                Double depositAmount = Double.parseDouble(scanner.nextLine());
                creditCard.deposit(depositAmount);
                break;
            }
            case 3: {
                System.out.print("Enter withdrawal amount -> ");
                Double withdrawalAmount = Double.parseDouble(scanner.nextLine());
                creditCard.withdrawal(withdrawalAmount);
                break;
            }
            case 4: {
                System.exit(0);
                break;
            }
            default: {
                System.out.println("Invalid options..!");
            }
        }
        System.out.println("Press Enter key to continue...");
        scanner.nextLine();
        System.out.print('\u000C');
    }

    public void start () {
        while (isProcessing) {
            getOption();
            doProcess();
        }
    }
}
