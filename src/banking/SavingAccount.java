package banking;

public class SavingAccount extends Account {
    float interest;

    public SavingAccount(String name, Integer number, Double balance) {
        super(name, number, balance);
        interest = -1F;
    }

    @Override
    public void withdrawal(Double amount) {

    }

    @Override
    public void deposit(Double amount) {
        System.out.println("Deposit saving account");
    }
}
