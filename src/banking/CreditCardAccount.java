package banking;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Scanner;

/**
 * Verify code for deposit
 */
public class CreditCardAccount extends Account {
    private Integer pin;
    private LocalDate issuedAt;
    private LocalDate thruAt;

    public CreditCardAccount() {
        super();
    }

    public CreditCardAccount(String name, Integer number, Double balance) {
        super(name, number, balance);
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    public LocalDate getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(LocalDate issuedAt) {
        this.issuedAt = issuedAt;
    }

    public LocalDate getThruAt() {
        return thruAt;
    }

    public void setThruAt(LocalDate thruAt) {
        this.thruAt = thruAt;
    }

    @Override
    public void deposit(Double amount) {
        if (super.getBalance() == null) {
            super.deposit(amount);
        }
    }

    // Use final to prevent override our withdrawal() method
    @Override
    public final void withdrawal(Double amount) {
        LocalDate now = LocalDate.now();
        if (now.isBefore(thruAt)) {
            if (Objects.equals(pin, Database.INSTANCE.getInstance().pin)) {
                super.withdrawal(amount);
            } else {
                System.out.println("Incorrect pinned");
            }
        } else {
            System.out.println("Expired");
        }
    }

    // Overloading
    // Use in case you want to withdrawal with all money you have
    // Use final to prevent override our withdrawal() method
    public final void withdrawal() {
        withdrawal(getBalance());
    }

    @Override
    public void showBalance() {
        super.showBalance();
        System.out.println("Pin: " + pin);
        System.out.println("Issued at: " + issuedAt);
        System.out.println("Thru at: " + thruAt);
    }
}
