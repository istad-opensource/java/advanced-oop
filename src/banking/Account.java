package banking;

import java.time.LocalDate;

public class Account {
    private String name;
    private Integer number;
    private Double balance;
    private Boolean overdraft;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getBalance() {
        return balance;
    }

    private void setBalance(Double balance) {
        this.balance = balance;
    }

    public Boolean getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(Boolean overdraft) {
        this.overdraft = overdraft;
    }

    public Account() {

    }

    public Account(String name, Integer number, Double balance) {
        this.name = name;
        this.number = number;
        this.balance = balance;
    }

    public void showBalance() {
        System.out.println("Account Number: " + this.number);
        System.out.println("Account Balance: " + this.balance);
    }

    public void deposit(Double amount) {
        if (amount < 0) {
            System.out.println("Are you crazy?");
            return;
        }
        if (amount >= 0) {
            balance += amount;
        }
    }

    public void withdrawal(Double amount) {
        if (amount > balance) {
            System.out.println("Insufficient Balance");
            return;
        }
        if (amount < 0) {
            System.out.println("Are you crazy?");
            return;
        }
        balance -= amount;
    }

}
